<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

// pour definir des liens vers
if (!defined('CODES_SPIP_BRANCHE')) {
	#define(CODES_SPIP_BRANCHE, '4.0');
	define('CODES_SPIP_BRANCHE', 'master');
}

/** 
 * Renvoie vers le fichier sur la forge git
 * 
 * [?ecrire/inc_version.php#core] 
 * [?plugins-dist/aide/paquet.xml#core] 
*/
function glossaire_core($chemin, $ligne=0) {
	// gestion des aiguillages automatiques
	// vers core_plugins lorsque le chemin indique un fichier qui y pointe.
	static $aiguillages = array(
		'master' => array(
			'plugins-dist' => '',
			'squelettes-dist' => 'dist',
		),
		'2.1' => array(
			'extensions' => '',
		),
	);
	$aiguillage = $aiguillages[CODES_SPIP_BRANCHE] ?? $aiguillages['master'];
	foreach ( $aiguillage as $repertoire => $remplacer ) {
		$len = strlen($repertoire);
		if (substr($chemin, 0, $len) === $repertoire) {
			return glossaire_spip_url('core_plugins', $remplacer . substr($chemin, $len), $ligne);
		}
	}

	// sinon c'est la...
	return glossaire_spip_url('core', $chemin, $ligne);
}

/** 
 * Renvoie vers le fichier sur la forge git (spip/)
 * 
 * [?mots/paquet.xml#core_plugins] 
*/
function glossaire_core_plugins($chemin, $ligne=0) {
	return glossaire_spip_url('zone_plugins', $chemin, $ligne);
}

/** 
 * Renvoie vers le fichier sur la forge git (spip-contrib-extensions/)
 * 
 * [?fabrique/paquet.xml#zone_plugins] 
*/
function glossaire_zone_plugins($chemin, $ligne=0) {
	return glossaire_spip_url('zone_plugins', $chemin, $ligne);
}

/**
 * Retourne l’url pour un fichier
 *
 * @param string $type
 * @param string $chemin
 * @param ?int $ligne
 * @return string
 */
function glossaire_spip_url(string $type='core', string $chemin, ?int $ligne = null): string {
	static $source = 'https://git.spip.net/@organization@/@project@/src/branch/@branch@/@file@';
	$chemin = ltrim($chemin, '/');

	if ($type === 'core' or $type === 'core_plugins') {
		$organization = 'spip';
	} else {
		$organization = 'spip-contrib-extensions';
	}
	if ($type === 'core') {
		$project = 'spip';
	} elseif (stripos($chemin, '/')) {
		list($project, $chemin) = explode('/', $chemin, 2);
	} else {
		$project = $chemin;
		$chemin = '';
	}

	$replacements = [
		'@organization@' => $organization,
		'@project@' => $project,
		'@branch@' => CODES_SPIP_BRANCHE,
		'@file@' => $chemin,
	];

	$url = str_replace(array_keys($replacements), $replacements, $source);

	// on remplace le chemin du fichier et ajoute l'eventuelle ligne.
	return $url . ($ligne ? '#L' . $ligne : '');
}
