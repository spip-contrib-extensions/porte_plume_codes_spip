<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/porte_plume_extras/codes/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pp_codes_spip_description' => 'Gestion de boutons pour insérer des liens pointant sur les sources en lignes de SPIP ou de la Zone.',
	'pp_codes_spip_nom' => 'Codes Informatiques SPIP pour Porte Plume',
	'pp_codes_spip_slogan' => 'Ajoute des boutons de liens vers les sources en ligne de SPIP'
);

?>
