<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/porte_plume_extras/codes/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// O
	'outil_inserer_lien_core' => 'Insérer un lien vers le code de SPIP',
	'outil_inserer_lien_plugins_core' => 'Insérer un lien vers le code des plugins de SPIP',
	'outil_inserer_lien_plugins_zone' => 'Insérer un lien vers le code des plugins de la Zone',
);

?>
